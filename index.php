<?php
/**
 * Created by PhpStorm.
 * User: InnerFlame
 * Date: 13.01.15
 * Time: 15:12
 */
header('Content-Type: application/json');


if(!empty($_GET['name'])){
    $name = $_GET['name'];
    $price = get_price($name);

    if(empty($price)){
        deliver_response(200, 'book not found', null);
    }else{
        deliver_response(200, 'book found', $price);
    }
}else{
    deliver_response(400, 'Invalid Reques', null);
}

function deliver_response($status,$status_message,$data){
    header("HTTP/1.1 $status $status_message");

    $response['status'] = $status;
    $response['status_message'] = $status_message;
    $response['data'] = $data;

    $res = json_encode($response);
    return $res;
}

function get_price($value){
    $books = array(
        'java' => 299,
        'c' => 399,
        'php' => 599
    );

    foreach($books as $book => $price){
        if($book == $value){
            return $price;
            break;
        }
    }
}